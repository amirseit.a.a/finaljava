package app.controller;


import app.model.Auth;
import app.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@AllArgsConstructor
public class AuthController {

    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Auth auth) {
        try {
            return ResponseEntity.ok(authService.login(auth));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping("/customers/me")
    public ResponseEntity<?> me(@RequestHeader("Auth") String token) throws Exception {
        return ResponseEntity.ok(authService.getCustomerByToken(token));
    }
    @GetMapping("/s")
    public ResponseEntity<?> getProduct() {
        return ResponseEntity.ok(authService.getAll());
    }
    @GetMapping("/mvc")
    public String greeting(Map<String,Object> model)
    {
        return "mvc";
    }
}
