package app.controller;

import app.model.CustomerOrder;
import app.service.CustomerOrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerOrderController {
    private final CustomerOrderService customerOrderService;

    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @GetMapping("/api/customerOrder/{customerId}")
    public ResponseEntity<?> getOrder(@PathVariable Long customerId) {
        System.out.println(customerId);
        return ResponseEntity.ok(customerOrderService.getByCustomerId(customerId));
    }


    @PostMapping("/api/customerOrder")
    public ResponseEntity<?> saveOrder(@RequestBody CustomerOrder customerOrder) {
        System.out.println(customerOrder.toString());
        return ResponseEntity.ok(customerOrderService.create(customerOrder));
    }

    @PostMapping("/api/customerOrder/delete/{customerOrderId}")
    public void deleteOrder(@PathVariable Long customerOrderId) {
        customerOrderService.delete(customerOrderId);
    }

}