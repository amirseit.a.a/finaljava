package app.controller;



import app.model.OrderItem;
import app.service.OrderItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderItemController {
    private final OrderItemService orderItemService;

    public OrderItemController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @GetMapping("/api/order/{customerOrderId}")
    public ResponseEntity<?> getOrder(@PathVariable Long customerOrderId) {
        return ResponseEntity.ok(orderItemService.getByCustomerOrderId(customerOrderId));
    }

    @PostMapping("/api/OrderItem")
    public ResponseEntity<?> saveOrder(@RequestBody OrderItem orderItem) {
        System.out.println(orderItem.toString());
        return ResponseEntity.ok(orderItemService.create(orderItem));
    }

}
