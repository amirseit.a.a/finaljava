package app.controller;

import app.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {

        this.productService = productService;
    }

    @GetMapping("/api/products")
    public ResponseEntity<?> getProduct() {
        return ResponseEntity.ok(productService.getAll());
    }



    @RequestMapping(value="/api/products/{categoryId}")
    public ResponseEntity<?> getProduct(@PathVariable Long categoryId) {
        return ResponseEntity.ok(productService.getAllByCategoryId(categoryId));
    }
}
