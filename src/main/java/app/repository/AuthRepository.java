package app.repository;


import app.model.Auth;
import app.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {


    Auth findByLoginAndPassword(String login, String password);

    Auth findByToken(String token);


    @Query(value = "SELECT * FROM Auth", nativeQuery = true)
    List<Auth> findAllByAuthId(Long auth_id);
}
