package app.repository;

import app.model.CustomerOrder;
import app.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerOrderRepository extends CrudRepository<CustomerOrder,Long> {


    @Query(value = "SELECT * FROM Customerorder WHERE customer_id = ?1", nativeQuery = true)
    List<CustomerOrder> findAllByCustomerId(Long customer_id);
}
