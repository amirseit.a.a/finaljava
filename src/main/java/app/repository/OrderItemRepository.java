package app.repository;


import app.model.CustomerOrder;
import app.model.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem,Long> {

    @Query(value = "SELECT * FROM orderitem WHERE customer_order_id = ?1", nativeQuery = true)
    List<OrderItem> findAllByCustomerOrderId(Long customerOrderId);
}