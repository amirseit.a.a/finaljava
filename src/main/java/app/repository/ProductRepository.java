package app.repository;

import app.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {

    @Query(value = "SELECT * FROM Product WHERE category_id = ?1", nativeQuery = true)
    List<Product> findAllByCategoryId(Long categoryId);
}

