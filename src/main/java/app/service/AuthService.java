package app.service;


import app.model.Auth;
import app.model.Customer;
import app.model.Product;
import app.repository.AuthRepository;
import app.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;


@Service
@AllArgsConstructor
public class AuthService {

    private final AuthRepository authRepository;
    private final CustomerRepository customerRepository;



    @Transactional
    public Auth login(Auth auth) throws Exception {

        Auth authDB = authRepository.findByLoginAndPassword(auth.getLogin(), auth.getPassword());

        if(authDB == null) {
            throw new Exception();
        }

        String token = UUID.randomUUID().toString();

        authDB.setToken(token);
        return authRepository.save(authDB);
    }

    public Customer getCustomerByToken(String token) throws Exception {

        Auth authDB = authRepository.findByToken(token);
        return customerRepository.findById(authDB.getCustomerId()).orElseThrow(ChangeSetPersister.NotFoundException::new);
    }


    public List<Auth> getAll(){
        return (List<Auth>) authRepository.findAll();
    }

    public List<Auth> getAllByAuthId(Long categoryId){
        return authRepository.findAllByAuthId(categoryId);
    }


}
