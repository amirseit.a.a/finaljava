package app.service;


import app.model.CustomerOrder;

import app.repository.CustomerOrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOrderService {
    public final CustomerOrderRepository customerOrderRepository;

    public CustomerOrderService(CustomerOrderRepository customerOrderRepository) {
        this.customerOrderRepository = customerOrderRepository;
    }
    public List<CustomerOrder> getAll(){
        return (List<CustomerOrder>) customerOrderRepository.findAll();
    }

    public CustomerOrder create(CustomerOrder customerOrder){
        return customerOrderRepository.save(customerOrder);
    }


    public List<CustomerOrder> getByCustomerId(Long customerId) {
        return (List<CustomerOrder>) customerOrderRepository.findAllByCustomerId(customerId);
    }

    public void delete(Long customerOrderId) {
        customerOrderRepository.deleteById(customerOrderId);
    }
}
