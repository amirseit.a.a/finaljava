package app.service;

import app.repository.CustomerRepository;
import app.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
}
