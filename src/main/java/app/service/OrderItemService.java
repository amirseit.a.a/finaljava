package app.service;



import app.model.OrderItem;

import app.repository.OrderItemRepository;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemService {
    public final OrderItemRepository orderItemRepository;

    public OrderItemService(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }
    public List<OrderItem> getAll(){
        return (List<OrderItem>) orderItemRepository.findAll();
    }

    public List<OrderItem> getByCustomerOrderId(Long customerOrderId) {
        return (List<OrderItem>) orderItemRepository.findAllByCustomerOrderId(customerOrderId);
    }
    public OrderItem create(OrderItem orderItem){
        return orderItemRepository.save(orderItem);
    }

}