package app.service;

import app.model.Product;
import app.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ProductService {
    public final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public List<Product> getAll(){
        return (List<Product>) productRepository.findAll();
    }

    public List<Product> getAllByCategoryId(Long categoryId){
        return productRepository.findAllByCategoryId(categoryId);
    }

}

