
DROP TABLE IF EXISTS Category;
CREATE TABLE Category (
	category_id serial primary key,
	title varchar default 255,
	description varchar default 255,
	storage_period_id int
);
alter table Category owner to postgres;


DROP TABLE IF EXISTS Product;
CREATE TABLE Product (
	product_id serial  primary key,
	title varchar default 255,
	description varchar default 255,
	price float,
	made_date date,
	category_id bigint
    		constraint product_category_id_fk
    			references Category (category_id)
);
 alter table Product owner to postgres;

 DROP TABLE IF EXISTS OrderItem;
 CREATE TABLE OrderItem (
                              orderItem_id serial NOT NULL,
                              quantity integer NOT NULL,
                              price FLOAT NOT NULL,
                              product_id integer NOT NULL,
                              customer_order_id integer NOT NULL,
                              CONSTRAINT "OrderItem_pk" PRIMARY KEY (orderItem_id)
 ) WITH (
       OIDS=FALSE
     );
 alter table OrderItem owner to postgres;
 ALTER TABLE OrderItem ADD CONSTRAINT OrderItem_fk0 FOREIGN KEY (product_id) REFERENCES Product(product_id);

 DROP TABLE IF EXISTS CustomerOrder;
 CREATE TABLE CustomerOrder (
                                  customerOrder_id serial NOT NULL,
                                  customer_id integer NOT NULL,
                                  total_price FLOAT NOT NULL,
                                  CONSTRAINT "CustomerOrder_pk" PRIMARY KEY (customerOrder_id)
 ) WITH (
       OIDS=FALSE
     );

 alter table CustomerOrder owner to postgres;
 ALTER TABLE OrderItem ADD CONSTRAINT OrderItem_fk1 FOREIGN KEY (customer_order_id) REFERENCES CustomerOrder(customerOrder_id);

DROP TABLE IF EXISTS Auth;
CREATE TABLE Auth (
                        auth_id serial NOT NULL,
                        customer_id integer NOT NULL,
                        login VARCHAR(255) NOT NULL,
                        role VARCHAR(255) NOT NULL,
                        password VARCHAR(255) NOT NULL,
                        token VARCHAR(255)  NULL,
                        CONSTRAINT Auth_pk PRIMARY KEY (auth_id)
) WITH (
      OIDS=FALSE
    );
alter table Auth owner to postgres;


DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
                      cus_id serial NOT NULL,
                      name  VARCHAR(255) NOT NULL,
                      description VARCHAR(255) NOT NULL,
                      phone VARCHAR(255) NOT NULL,
                      email VARCHAR(255) NOT NULL,
                      CONSTRAINT customer_pk PRIMARY KEY (cus_id)
) WITH (
      OIDS=FALSE
    );
alter table customer owner to postgres;

ALTER TABLE Auth ADD CONSTRAINT Auth_fk1 FOREIGN KEY (customer_id) REFERENCES customer(cus_id);
ALTER TABLE CustomerOrder ADD CONSTRAINT CustomerOrder_fk0 FOREIGN KEY (customer_id) REFERENCES customer(cus_id);