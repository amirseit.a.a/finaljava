INSERT INTO Category (category_id,title,description,storage_period_id)
VALUES(1,'Milk Chocolate','a solid chocolate confectionery that contains cocoa, sugar and milk',1),
(2,'White Chocolate','made of sugar, milk, and cocoa butter, without the cocoa solids',1),
(3,'Dark Chocolate','a form of chocolate containing cocoa solids, cocoa butter, without the milk or butter found in milk chocolate',2);


INSERT INTO Product (product_id,title,description,price,category_id,made_date)
VALUES(01,'Coconut milk chocolate','Based on coconut plant milk, coconut water and meat of coconut',204.50,1,'2020-10-11'),
(02,'Cow milk chocolate','Based on cow dairy milk with lactose and casein',200.30,1,'2019-12-20'),
(03,'Bubbled chocolate','Oxygenated chocolate',190.50,2,'2020-09-01'),
(04,'Ordinary chocolate','Pressed form of a chocolate',243.50,2,'2020-05-12'),
(05,'On stevia Dark Chocolate','Sugar free chocolate sweetened by stevia  ',195.50,3,'2020-01-23'),
(06,'On sugar Dark Chocolate','Chocolate sweetened by sugar',100.50,3,'2020-03-13');


INSERT INTO customer(cus_id,name,description,phone,email)
VALUES (1,'Dimash','test','87479110694','d.kairatov@astanait.edu.kz');

INSERT INTO Auth(auth_id,customer_id,login,role,password)
VALUES (1,1,'admin','test','123'),
       (2,1,'test','test','pass');




