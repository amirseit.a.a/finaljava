var app = angular.module('aitu-project', []);

app.controller('CategoryCtrl', function($scope, $http){
    $scope.auth = {
        login: '',
        password: ''
    };
    $scope.customer = {};
    $scope.loginTest = function(auth) {
        $http({
            url: 'http://127.0.0.1:8085/login',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            data: $scope.auth
        })
            .then(function (response) {
                    $scope.auth = response.data;

                    $scope.getMe();

                },
                function (response) { // optional
                    $scope.auth = {};
                });
    };

    $scope.getMe = function() {
        $http({
            url: 'http://127.0.0.1:8085/customers/me',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            }
        })
            .then(function (response) {
                    $scope.customer = response.data;
                    $scope.getCategories();
                },
                function (response) { // optional
                    console.log(response);
                    $scope.customer = {};

                });
    };

    $scope.categoryList = [];
    $scope.getCategories = function() {
        $http({
            url: 'http://127.0.0.1:8085/api/categories',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            }

        })
            .then(function (response){
                    $scope.categoryList = response.data;
                },
                function (response) {
                    console.log(response);
                });

    };

    $scope.productList = {};
    $scope.getProductsByCategoryId = function(categoryId) {
        $http({
            url: 'http://127.0.0.1:8085/api/products/'+categoryId,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            }

        })
            .then(function (response){
                    $scope.productList[categoryId] = response.data;
                    console.log($scope.productList);
                },
                function (response) {
                    console.log(response);
                });
    }

    $scope.hideProducts = function(categoryId) {
        delete $scope.productList[categoryId]
    }

    $scope.orderItemList = {};
    $scope.incrementProduct = function (product) {
        if($scope.orderItemList[product.productId] === undefined) $scope.orderItemList[product.productId] = {productId: product.productId, price: product.price, quantity: 0};
                $scope.orderItemList[product.productId].quantity = $scope.orderItemList[product.productId].quantity + 1;
    };

    $scope.sendOrders = function () {

        let totalPrice = 0;
        angular.forEach($scope.orderItemList, function (value) {
            value.productId
            totalPrice += value.price * value.quantity;
        });
        console.log(totalPrice);
       angular.forEach($scope.orderItemList, function(value, key){


           $http({
                    url: 'http://127.0.0.1:8085/api/customerOrder',
                    method: "POST",
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Content-Type": "application/json",
                        "Auth": $scope.auth.token
                    },
                    data: {customer_id:$scope.customer.cus_id,
                            total_price: totalPrice}
                })
                    .then(function (response){
                        console.log(response.data);

                        $scope.getOrders();
                    },
                    function (response) {
                        console.log(response);
                    });
       });

    };


    $scope.Orders = [];
    $scope.getOrders = function() {
        $http({
                url: 'http://127.0.0.1:8085/api/customerOrder/'+$scope.customer.cus_id,
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "Auth": $scope.auth.token
                }

            })
                .then(function (response){
                    $scope.Orders = response.data;
                    console.log($scope.Orders);
                },
                function (response) {
                    console.log($http.categoryId);
                    console.log(response);
                });
    };



$scope.sendOrdersItem = function () {
    let proId
    angular.forEach($scope.orderItemList, function (value) {
        proId=value.productId
    });

    angular.forEach($scope.orderItemList, function(value, key){
        $http({
            url: 'http://127.0.0.1:8085/api/OrderItem',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            },
            data: {
                    quantity: value.quantity,
                    price: value.price,
                    product_id: proId,
                    customer_order_id:1}
        })
            .then(function (response){
                    console.log(response.data);
                    $scope.getOrdersItem();
                },
                function (response) {
                    console.log(response);
                });
    });

};

$scope.OrdersItem = [];
$scope.getOrdersItem = function() {
    $http({
        url: 'http://127.0.0.1:8085/api/OrderItem/'+$scope.customer.cus_id,
        method: "GET",
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
            "Auth": $scope.auth.token
        }

    })
        .then(function (response){
                $scope.OrdersItem = response.data;
                console.log($scope.OrdersItem);
            },
            function (response) {
                console.log($http.categoryId);
                console.log(response);
            });
};

    $scope.deleteOrder = function(customerorder_id) {
        $http({
            url: 'http://127.0.0.1:8085/api/customerOrder/delete/'+customerorder_id,
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth": $scope.auth.token
            }

        })
            .then(function (response){
                console.log(customerorder_id);
                    console.log(response.data);

                },
                function (response) {
                    console.log(response);
                });
    };




    $scope.deleteOrderItem = function(productId) {

        /* $scope.orderItemList[productId]=undefined;*/

         delete $scope.orderItemList[productId]

    };

});

